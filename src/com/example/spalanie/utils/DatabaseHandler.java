package com.example.spalanie.utils;

import com.example.spalanie.models.Consumption;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHandler extends SQLiteOpenHelper {

	private static final int DATABASE_VERSION = 3;
	private static final String DATABASE_NAME = "spalanie.db";
	private static DatabaseHandler instance;

	public static synchronized DatabaseHandler getHelper(Context context) {
		if (instance == null) {
			instance = new DatabaseHandler(context);
		}
		return instance;
	}

	private DatabaseHandler(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		createTables(db);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		dropTables(db);
		createTables(db);
	}

	private static void dropTables(SQLiteDatabase db) {
		db.execSQL("DROP TABLE IF EXISTS `" + Consumption.TABLE_NAME + "`");
	}

	private static void createTables(SQLiteDatabase db) {
		db.execSQL(Consumption.getCreateSqlTable());
	}

}
