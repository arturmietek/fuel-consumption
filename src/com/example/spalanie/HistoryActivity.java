package com.example.spalanie;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ListView;

import com.example.spalanie.adapters.HistoryListAdapter;
import com.example.spalanie.models.Consumption;
import com.example.spalanie.utils.DatabaseHandler;

public class HistoryActivity extends ActionBarActivity implements OnItemLongClickListener {
	
	HistoryListAdapter adapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_history);

		adapter = new HistoryListAdapter(this, Consumption.getCursor(DatabaseHandler.getHelper(this)));

		ListView listView = (ListView) findViewById(R.id.list);
		listView.setAdapter(adapter);
		listView.setOnItemLongClickListener(this);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			this.finish();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
		AlertDialog delConfirmDialog = DeleteConfirmationDialog(id);
		delConfirmDialog.show();
		return false;
	}

	private AlertDialog DeleteConfirmationDialog(final long id) {
		
		AlertDialog deleteDialogBox = new AlertDialog.Builder(this)
		// set message, title, and icon
				.setTitle(R.string.delete).setMessage(R.string.do_you_want_to_delete).setIcon(android.R.drawable.ic_delete)
				.setPositiveButton(R.string.delete, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						Consumption.remove(DatabaseHandler.getHelper(HistoryActivity.this), id);
						adapter.changeCursor(Consumption.getCursor(DatabaseHandler.getHelper(HistoryActivity.this)));
						dialog.dismiss();
					}

				})
				.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				}).create();
		return deleteDialogBox;
	}

}
