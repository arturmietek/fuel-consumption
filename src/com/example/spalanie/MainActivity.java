package com.example.spalanie;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.spalanie.models.Consumption;
import com.example.spalanie.utils.DatabaseHandler;
import com.example.views.FuelingGauge;

public class MainActivity extends ActionBarActivity implements TextWatcher, OnClickListener {

	private static final String PREFERENCE_KEY_LAST_DISTANCE = "lastDistance";
	private static final String PREFERENCE_KEY_LAST_FUEL = "lastFuel";
	private EditText etFuel;
	private EditText etDistance;
	private EditText etDescription;
	private ImageView bAddToDatabase;
	private Consumption mConsumption;
	private boolean calculated;
	private FuelingGauge fuelinggGauge;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		mConsumption = new Consumption();
		
		
		etFuel = (EditText) findViewById(R.id.fuel);
		etDistance = (EditText) findViewById(R.id.distance);
		etDescription = (EditText) findViewById(R.id.description);
		
		
		int count = Consumption.getTotalCount(DatabaseHandler.getHelper(this));
		int minFueling = 3;
		int maxFueling = 15;
		if (count > 1) {
			float[] stats = Consumption.getStats(DatabaseHandler.getHelper(this));
			minFueling = (int)Math.floor(stats[Consumption.STATS_FILEDS.MIN_CONSUMPTION]);
			maxFueling = (int)Math.ceil(stats[Consumption.STATS_FILEDS.MAX_CONSUMPTION]);
		}
		
		fuelinggGauge = (FuelingGauge) findViewById(R.id.fueling_gauge);
		fuelinggGauge.setValues(minFueling, maxFueling, 0);
		

		// restore
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
		String textFuel = prefs.getString(PREFERENCE_KEY_LAST_FUEL, "");
		String textDistance = prefs.getString(PREFERENCE_KEY_LAST_DISTANCE, "");
		if (!etDistance.equals("") && !etFuel.equals("")) {
			etFuel.setText(textFuel);
			etDistance.setText(textDistance);
			calculate(textFuel, textDistance);
		}

		etFuel.addTextChangedListener(this);
		etDistance.addTextChangedListener(this);

		bAddToDatabase = (ImageView) findViewById(R.id.add_to_database);
		bAddToDatabase.setOnClickListener(this);
		
		
		
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		Intent intent;
		switch (item.getItemId()) {
		case R.id.action_history:
			intent = new Intent(this, HistoryActivity.class);
			startActivity(intent);
			return true;

		case R.id.action_fueling_calculation:
			intent = new Intent(this, FuelingActivity.class);
			startActivity(intent);
			return true;
			
		case R.id.action_statistics:
			intent = new Intent(this, StatisticsActivity.class);
			startActivity(intent);
			return true;
			
		default:
			break;
		}
		
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count, int after) {
	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
	}

	@Override
	public void afterTextChanged(Editable s) {
		String textFuel = etFuel.getText().toString();
		String textDistance = etDistance.getText().toString();

		calculate(textFuel, textDistance);
		save(textFuel, textDistance);
		calculated = true;
	}
	
	private void save(String textFuel, String textDistance) {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
		SharedPreferences.Editor editor = prefs.edit();
		editor.putString(PREFERENCE_KEY_LAST_FUEL, textFuel);
		editor.putString(PREFERENCE_KEY_LAST_DISTANCE, textDistance);
		editor.commit();
	}
	
	private void calculate(String textFuel, String textDistance) {
		if (textFuel.length() > 0 && textDistance.length() > 0) {
			float distance = Float.valueOf(textDistance);
			float fuel = Float.valueOf(textFuel);
			float consumption = fuel * 100f / distance;
			
			mConsumption.distance = (int) distance;
			mConsumption.fuel = fuel;
			mConsumption.consumption = consumption;
			mConsumption.description = etDescription.getText().toString();
			
			fuelinggGauge.setValues((float)Math.round(consumption * 10) / 10);
			
		} else {
			fuelinggGauge.setValues(0);
		}
		
	}
	

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.add_to_database:
			if (calculated) {
				Consumption.add(DatabaseHandler.getHelper(this), mConsumption);
				calculated = false;
				Toast.makeText(this, getText(R.string.consumption_added), Toast.LENGTH_LONG).show();
			} else {
				Toast.makeText(this, getText(R.string.consumption_not_calculated), Toast.LENGTH_LONG).show();
			}
			
			break;
		}

	}
}
