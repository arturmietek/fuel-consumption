package com.example.spalanie;

import java.text.NumberFormat;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;

import com.example.spalanie.models.Consumption;
import com.example.spalanie.utils.DatabaseHandler;

public class FuelingActivity extends ActionBarActivity implements TextWatcher {

	private EditText etFuel;
	private EditText etDistance;
	private TextView tvResult;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_fueling);

		tvResult = (TextView) findViewById(R.id.result);
		
		etFuel = (EditText) findViewById(R.id.fuel);
		etDistance = (EditText) findViewById(R.id.distance);
		
		float[] stats = Consumption.getStats(DatabaseHandler.getHelper(this));
		
		etFuel.setText(String.valueOf(stats[Consumption.STATS_FILEDS.AVERAGE_CONSUMPTION]));
		
	
		etFuel.addTextChangedListener(this);
		etDistance.addTextChangedListener(this);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			this.finish();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count, int after) {
	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
	}

	@Override
	public void afterTextChanged(Editable s) {
		String textFuel = etFuel.getText().toString();
		String textDistance = etDistance.getText().toString();
		
		calulcate(textFuel, textDistance);
		
	}

	private void calulcate(String textFuel, String textDistance) {
		if (textFuel.length() > 0 && textDistance.length() > 0) {
			float distance = Float.valueOf(textDistance);
			float fuel = Float.valueOf(textFuel);
			float liters = distance / 100f * fuel;
			
			NumberFormat format = NumberFormat.getNumberInstance();
			
			tvResult.setText(format.format((float)Math.ceil(liters * 10) / 10) + " l");
			
		} else {
			tvResult.setText("");
		}
		
	}

}
