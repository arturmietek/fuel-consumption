package com.example.spalanie.adapters;

import java.text.NumberFormat;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.spalanie.R;
import com.example.spalanie.models.Consumption;
import com.example.spalanie.utils.Utils;

public class HistoryListAdapter extends CursorAdapter {

	NumberFormat format;

	public HistoryListAdapter(Context context, Cursor c) {
		super(context, c, false);

		format = NumberFormat.getNumberInstance();
	}

	private static class ViewHolder {
		public TextView date;
		public TextView consumption;
		public TextView distance;
		public TextView description;
	}

	@Override
	public void bindView(View view, Context context, Cursor cursor) {
		ViewHolder holder = (ViewHolder) view.getTag();

		float consumption = Float.valueOf(cursor.getString(cursor.getColumnIndex(Consumption.CONSUMPTION)));

		consumption = (float) Math.round(consumption * 10) / 10;

		holder.date.setText(Utils.getDateFromUnixtime(cursor.getInt(cursor.getColumnIndex(Consumption.TIMESTAMP))));
		Utils.setTextWithUnit(holder.consumption, format.format(consumption), "l/100km");
		Utils.setTextWithUnit(holder.distance, cursor.getString(cursor.getColumnIndex(Consumption.DISTANCE)), "km");
		holder.description.setText(cursor.getString(cursor.getColumnIndex(Consumption.DESCRIPTION)));

		view.setId(cursor.getInt(cursor.getColumnIndex(Consumption.ID)));
	}

	public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
		View view = LayoutInflater.from(context).inflate(R.layout.activity_history_list_item, viewGroup, false);

		ViewHolder holder = new ViewHolder();
		holder.date = (TextView) view.findViewById(R.id.date);
		holder.consumption = (TextView) view.findViewById(R.id.consumption);
		holder.distance = (TextView) view.findViewById(R.id.distance);
		holder.description = (TextView) view.findViewById(R.id.description);
		view.setTag(holder);

		return view;
	}

}
